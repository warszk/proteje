using System;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Proteje.Editor.UiElements {
  public class PortObjectField : PortField<UnityEngine.Object>, IPortField {
    ObjectField field;
    Type type;

    public PortObjectField(UnityEngine.Object val, Type type) {
      this.type = type;
      Initialize(val);
    }

    public List<VisualElement> GetElements() {
      elements.Clear();
      elements.Add(new Label(" "));
      elements.Add(field);

      return elements;
    }
    protected override void Initialize(UnityEngine.Object initialValue) {
      field = new ObjectField
      {
        name = string.Empty,
        value = initialValue,
        objectType = type
      };

      field.RegisterValueChangedCallback((e) => {
        OnValueChanged?.Invoke(field.value);
      });
    }

    public void SetValue(object value)
    {
      // field.value = val;
    }
  }
}
