using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Proteje.Attributes;

namespace Proteje.Elements
{
  [Serializable]
  public abstract class YNode : ScriptableObject
  {
    public String guid;
    public Vector2 position = Vector2.zero;
    public YNodePortDictionary ports = new YNodePortDictionary();
    public Action<string> OnValueChanged;
    private YGraph _graph;
    public YGraph graph { set => _graph = value;  }

    private bool isStartNode = true;
    public bool IsStartNode { get => isStartNode; }
    public abstract void Exec();

    public static YNode Create(Type type) {
      var node = ScriptableObject.CreateInstance(type) as YNode;
      node.guid = Guid.NewGuid().ToString();
      node.InitializePorts();

      return node;
    }

    public void SetValue<T>(string portName, T val) {
      if(GetType().GetField(portName) != null)
        GetType().GetField(portName).SetValue(this, val);
      if((GetType().GetProperty(portName) != null))
        GetType().GetProperty(portName).SetValue(this, val);

      OnValueChanged?.Invoke(portName);
      if(ports[portName].IsOutput) {
        ports[portName].UpdateConnections(val);
      } else {
        Exec();
      }
    }

    public object GetValue(string portName)
    {
      if (GetType().GetField(portName) != null)
        return GetType().GetField(portName).GetValue(this);
      if ((GetType().GetProperty(portName) != null))
        return GetType().GetProperty(portName).GetValue(this);
      return null;
    }

    public Type GetValueType(string portName) {
      var field = GetType().GetField(portName);

      if (field != null)
      {
        return field.FieldType;
      }

      var prop = GetType().GetProperty(portName);
      if (prop != null)
      {
        return prop.PropertyType;
      }

      return null;
    }

    public bool ConnectWith(string outPortName, YNode inNode, string inPortName) {
      if (ports[outPortName].CanConnect(inNode.ports[inPortName]))
      {
        ports[outPortName].Connect(inNode.ports[inPortName]);
        inNode.ConnectTo(inPortName, this, outPortName);

        return true;
      }
      return false;
    }

    private void ConnectTo(string inPortName, YNode outNode, string outPortName) {
      if (ports[inPortName].IsInput)
      {
        ports[inPortName].Connect(outNode.ports[outPortName]);
        isStartNode = false;
      }
    }

    public void Disconnect(string outPortName, YNode inNode, string inPortName) {
      ports[outPortName].Disconnect(inNode.ports[inPortName]);
      inNode.ports[inPortName].Disconnect(ports[outPortName]);
      inNode.DisconnectFrom(inPortName, this, outPortName);

    }

    private void DisconnectFrom(string inPortName, YNode outNode, string outPortName)
    {
      if (ports[inPortName].IsInput)
      {
        ports[inPortName].Disconnect(outNode.ports[outPortName]);

        bool startNode = true;
        foreach (var kvp in ports)
        {
          if(kvp.Value.IsInput && kvp.Value.HasConnections) {
            startNode = false;
          }
        }
        isStartNode = startNode;
      }

    }

    protected void InitializePorts() {
      var flds = GetType().GetFields();

      foreach (var prop in flds)
      {
        var attrs = prop.GetCustomAttributes();
        HandleAttributes(prop.Name, attrs);
      }

      var props = GetType().GetProperties();
      foreach (var prop in props)
      {
        var attrs = prop.GetCustomAttributes();
        HandleAttributes(prop.Name, attrs);
      }

    }

    private void HandleAttributes(string portName, IEnumerable<Attribute> attrs) {
      foreach (var attr in attrs)
      {
        if (attr is InPortAttribute)
        {
          var a = (InPortAttribute)attr;
          ports.Add(portName, new YPort(this, portName, PortIO.In, a.type, a.connectorVisible));
        }
        else if (attr is OutPortAttribute)
        {
          var a = ((OutPortAttribute)attr);
          ports.Add(portName, new YPort(this, portName, PortIO.Out, a.type, a.connectorVisible));
        }
      }
    }

    [Serializable]
    public class YNodePortDictionary : Dictionary<string, YPort>, ISerializationCallbackReceiver
    {
      [SerializeField] private List<string> keys = new List<string>();
      [SerializeField] private List<YPort> values = new List<YPort>();

      public void OnAfterDeserialize()
      {
        this.Clear();

        if (keys.Count != values.Count)
          throw new System.Exception("there are " + keys.Count + " keys and " + values.Count + " values after deserialization. Make sure that both key and value types are serializable.");

        for (int i = 0; i < keys.Count; i++)
          this.Add(keys[i], values[i]);
      }

      public void OnBeforeSerialize()
      {
        keys.Clear();
        values.Clear();

        foreach (KeyValuePair<string, YPort> pair in this)
        {
          keys.Add(pair.Key);
          values.Add(pair.Value);
        }
      }
    }
  }
}
