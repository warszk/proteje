namespace Proteje.Attributes
{

  [System.Serializable]
  public enum ConnectionCapacity
  {
    Single,
    Multi,
  }
}
