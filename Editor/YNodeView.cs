using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using Proteje.Editor.UiElements;
using Proteje.Elements;

namespace Proteje.Editor
{
  public class YNodeView : Node
  {
    public YNode node;
    private List<Port> inPorts = new List<Port>();
    private List<Port> outPorts = new List<Port>();

    Dictionary<string, IPortField> fields = new Dictionary<string, IPortField>();

    public void Init()
    {
      node.OnValueChanged += OnNodeValueChanged;
      SetVisualData();
      InstantiatePorts();
      GeneratePorts();
    }

    private void SetVisualData()
    {
      var cname = node.GetType().ToString().Replace(".", "");
      AddToClassList(cname);
      RefreshExpandedState();
    }

    private void InstantiatePorts()
    {
      foreach (var key in node.ports.Keys)
      {
        var p = node.ports[key];
        var direction = p.IsInput ? Direction.Input : Direction.Output;
        var capacity = p.IsSingle ? Port.Capacity.Single : Port.Capacity.Multi;
        var port = InstantiatePort(Orientation.Horizontal, direction, capacity, p.GetValueType());
        port.portName = key;
        if(!p.IsConnectorVisible) port.RemoveAt(0);
        PortInitializer(port, p.GetValueType(), p.GetValue());
        if(p.IsInput) inPorts.Add(port);
        else outPorts.Add(port);
      }
    }

    private void GeneratePorts()
    {
      inputContainer.Clear();
      foreach (var port in inPorts)
      {
        inputContainer.Add(port);
      }

      outputContainer.Clear();
      foreach (var port in outPorts)
      {
        outputContainer.Add(port);
      }

      RefreshExpandedState();
      RefreshPorts();
    }

    // TODO: This should be refactored into something simillar to:
    // var pp = new PortField(); pp.OnValueChanged += (val) => node.SetValue(port.portName, val);
    // if(valueType == typeof(System.Int32)) ((PortIntField)pp).InitializeWith((int)value);
    // foreach (var fld in pp.GetElements())
    // {
    //    port.contentContainer.Add(fld);
    // }
    // pp.OnValueChaged += (val) => node.SetValue(port.portName, val);

    Port PortInitializer(Port port, Type valueType, object value)
    {
      if (valueType == typeof(System.Int32))
      {
        var pp = new PortIntField((int) value);
        foreach(var fld in pp.GetElements()) {
          port.contentContainer.Add(fld);
        }
        fields.Add(port.portName, pp);
        pp.OnValueChanged += (val) => node.SetValue(port.portName, val);
      } else if (valueType == typeof(Boolean)) {
        var pp = new PortIntField((int)value);
        foreach (var fld in pp.GetElements())
        {
          port.contentContainer.Add(fld);
        }
        fields.Add(port.portName, pp);
        pp.OnValueChanged += (val) => node.SetValue(port.portName, val);
      } else {
        Debug.Log(valueType);

        var pp = new PortObjectField((UnityEngine.Object)value, valueType);
        foreach (var fld in pp.GetElements())
        {
          port.contentContainer.Add(fld);
        }
        fields.Add(port.portName, pp);
        pp.OnValueChanged += (val) => node.SetValue(port.portName, val);
      }
      return port;
    }

    public override void HandleEvent(EventBase evt)
    {
      node.position = GetPosition().position;
      EditorUtility.SetDirty(node);

      base.HandleEvent(evt);
    }

    private void OnNodeValueChanged(string portName) {
      var val = node.GetValue(portName);
      fields[portName].SetValue(val);
    }
  }
}
