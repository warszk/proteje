using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Proteje.Editor.UiElements
{
  public class PortFloatField : PortField<System.Single>, IPortField {
    FloatField field;

    public PortFloatField(float val) {
      Initialize(val);
    }

    public List<VisualElement> GetElements() {
      elements.Clear();
      elements.Add(new Label(" "));
      elements.Add(field);

      return elements;
    }

    protected override void Initialize(float initialValue) {
      field = new FloatField
      {
        name = string.Empty,
        value = initialValue
      };

      field.RegisterCallback<FocusOutEvent>(
        (e) =>
        {
          OnValueChanged?.Invoke(field.value);
        }
      );
    }

    public void SetValue(object value)
    {
      field.value = (float)value;
    }
  }
}
