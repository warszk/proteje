namespace Proteje.Elements
{
  [System.Serializable]
  public class YEdge
  {
    public YNode inNode;
    public string inPort;
    public YNode outNode;
    public string outPort;
  }
}
