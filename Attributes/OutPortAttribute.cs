using System;

namespace Proteje.Attributes
{
  [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
  public class OutPortAttribute : Attribute
  {
    public ConnectionCapacity type;
    public bool connectorVisible;

    public OutPortAttribute(ConnectionCapacity type = ConnectionCapacity.Multi, bool connectorVisible = true)
    {
      this.type = type;
      this.connectorVisible = connectorVisible;
    }
  }
}
