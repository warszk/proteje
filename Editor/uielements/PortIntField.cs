using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Proteje.Editor.UiElements {
  public class PortIntField : PortField<int>, IPortField {
    IntegerField field;

    public PortIntField(int val) {
      Initialize(val);
    }

    public List<VisualElement> GetElements() {
      elements.Clear();
      elements.Add(new Label(" "));
      elements.Add(field);

      return elements;
    }
    protected override void Initialize(int initialValue) {
      field = new IntegerField
      {
        name = string.Empty,
        value = initialValue
      };

      field.RegisterCallback<FocusOutEvent>(
        (e) =>
        {
          OnValueChanged?.Invoke(field.value);
        }
      );
    }

    public void SetValue(object value)
    {
      Debug.Log("SetVal" + value);
      field.value = (int)value;
    }
  }
}
