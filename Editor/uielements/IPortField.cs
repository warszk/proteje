namespace Proteje.Editor.UiElements {
  interface IPortField
  {
    void SetValue(object value);
  }
}
