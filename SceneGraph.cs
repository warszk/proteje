using UnityEngine;
namespace Proteje
{
  public class SceneGraph : MonoBehaviour
  {
    public YGraph graph;
    public GameObject Context;

    void Awake() {
      Context = GetComponent<GameObject>();
    }
  }

  public class SceneGraph<T> : SceneGraph where T : YGraph
  {
    public new T graph { get { return base.graph as T; } set { base.graph = value; base.graph.Context = this; } }
  }

}
