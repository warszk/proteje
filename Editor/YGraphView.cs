using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using Proteje.Elements;

namespace Proteje.Editor
{
  public class YGraphView : GraphView
  {
    public Dictionary<string, Type> menuItems = new Dictionary<string, Type>();
    public YGraph baseGraph;

    public YGraphView()
    {
      styleSheets.Add(Resources.Load<StyleSheet>("PTEGraphStyles"));
      // styleSheets.Add(Resources.Load<StyleSheet>("NodeStyle"));
      SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);

      this.AddManipulator(new ContentDragger());
      this.AddManipulator(new SelectionDragger());
      this.AddManipulator(new RectangleSelector());
      this.AddManipulator(new FreehandSelector());

      graphViewChanged += OnGraphViewChanged;

      var grid = new GridBackground();
      Insert(0, grid);
      grid.StretchToParentSize();
    }

    public void Execute() {
      baseGraph.Execute();
    }

    public void Load()
    {
      Dictionary<string, YNodeView> _nodes = new Dictionary<string, YNodeView>();

      foreach (var bnode in baseGraph.nodes)
      {
        Debug.Log(bnode);
        var node = new YNodeView
        {
          node = bnode,
          title = bnode.name
        };

        node.Init();

        node.SetPosition(new Rect(bnode.position, new Vector2(200, 200)));
        AddElement(node);
        node.RefreshExpandedState();

        _nodes.Add(bnode.guid, node);
      }

      foreach (var edge in baseGraph.edges)
      {
        var inPort = ports.ToList().Find((p) => p.portName == edge.inPort && (((YNodeView)p.node).node == edge.inNode));
        var outPort = ports.ToList().Find((p) => p.portName == edge.outPort && (((YNodeView)p.node).node == edge.outNode));
        var inNode = inPort.node as YNodeView;
        var outNode = outPort.node as YNodeView;

        var tmpEdge = new Edge
        {
          output = outPort,
          input = inPort
        };
        tmpEdge.output.Connect(tmpEdge);
        Add(tmpEdge);
      }
    }

    private GraphViewChange OnGraphViewChanged(GraphViewChange graphViewChange)
    {
      if (graphViewChange.edgesToCreate != null)
      {
        foreach (var edge in graphViewChange.edgesToCreate)
        {
          AddConnection(edge);
        }
      }

      if (graphViewChange.elementsToRemove != null)
      {
        foreach (var element in graphViewChange.elementsToRemove)
        {
          var type = element.GetType();
          if (type.Equals(typeof(Edge)))
          {
            RemoveConnection(element as Edge);
          }
          else if (type.Equals(typeof(YNodeView)))
          {
            RemoveNode(element as YNodeView);
          }
        }
      }

      return graphViewChange;
    }

    public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
    {
      var compatiblePorts = new List<Port>();
      ports.ForEach((port) =>
      {
        if (startPort != port && startPort.node != port.node && port.portType == startPort.portType && startPort.direction != port.direction)
        {
          compatiblePorts.Add(port);
        }

      });
      return compatiblePorts;
    }

    private void AddConnection(Edge edge) {
      var inNode = ((YNodeView)edge.input.node).node;
      var outNode = ((YNodeView)edge.output.node).node;
      baseGraph.Connect(outNode, edge.output.portName, inNode, edge.input.portName);
      EditorUtility.SetDirty(baseGraph);
    }

    private void RemoveConnection(Edge edge)
    {
      var inNode = edge.input.node as YNodeView;
      var outNode = edge.output.node as YNodeView;
      baseGraph.Disconnect(outNode.node, edge.output.portName, inNode.node, edge.input.portName);
      EditorUtility.SetDirty(baseGraph);
    }

    private void AddNode(YNodeView nodeView) {
      baseGraph.Add(nodeView.node);
      EditorUtility.SetDirty(baseGraph);
      AssetDatabase.AddObjectToAsset(nodeView.node, baseGraph);
    }

    private void RemoveNode(YNodeView nodeView) {
      baseGraph.Remove(nodeView.node);
      EditorUtility.SetDirty(baseGraph);
      AssetDatabase.RemoveObjectFromAsset(nodeView.node);
    }

    public YNodeView InsertYNodeView(Type nodeType, Vector2 pos)
    {
      var bnode = YNode.Create(nodeType);
      var _names = nodeType.ToString().Split('.');
      bnode.name = ObjectNames.NicifyVariableName(_names[_names.Length - 1]);

      bnode.position = pos;

      var node = new YNodeView
      {
        node = bnode,
        title = bnode.name
      };

      node.Init();
      node.SetPosition(new Rect(pos, new Vector2(200, 200)));

      AddElement(node);

      node.RefreshExpandedState();

      AddNode(node);
      return node;
    }


    public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
    {
      var pos = evt.mousePosition;

      foreach (KeyValuePair<string, Type> kvp in menuItems)
      {
        evt.menu.AppendAction(kvp.Key, a => { InsertYNodeView(kvp.Value, pos); }, a => DropdownMenuAction.Status.Normal, null);
      }

      base.BuildContextualMenu(evt);
    }
  }
}
