using UnityEngine;
using UnityEditor;

namespace Proteje.Editor
{
  [CustomEditor(typeof(YGraph), true)]
  public class YGraphAssetEditor : UnityEditor.Editor
  {
    YGraph graph;
    void OnEnable()
    {
      graph = (YGraph)target;
    }

    public override void OnInspectorGUI()
    {
      if (GUILayout.Button("Open"))
      {
        YGraphEditorWindow.Load(graph);
      }

      if (GUILayout.Button("Save"))
      {
        EditorUtility.SetDirty(graph);
        AssetDatabase.SaveAssets();
      }

      var property = serializedObject.FindProperty("nodes");
      serializedObject.Update();
      EditorGUILayout.PropertyField(property, true);
      serializedObject.ApplyModifiedProperties();

      var property2 = serializedObject.FindProperty("edges");
      serializedObject.Update();
      EditorGUILayout.PropertyField(property2, true);
      serializedObject.ApplyModifiedProperties();
    }
  }
}
