using System;
using System.Collections.Generic;
using UnityEngine;
using Proteje.Attributes;

namespace Proteje.Elements
{
  [Serializable]
  public class YPort
  {
    [SerializeField] private List<YPort> connections = new List<YPort>();
    [SerializeField] private String guid;
    [SerializeField] private YNode node;
    [SerializeField] private string portName;
    [SerializeField] private ConnectionCapacity capacity;
    [SerializeField] private bool isConnectorVisible;
    [SerializeField] private PortIO direction;

    public YPort(YNode n, string portName, PortIO direction, ConnectionCapacity capacity, bool isConnectorVisible, string guid = null) {
      node = n;
      this.portName = portName;
      this.direction = direction;
      this.capacity = capacity;
      this.isConnectorVisible = isConnectorVisible;
      this.guid = guid == null ? Guid.NewGuid().ToString() : guid;
    }

    public void Connect(YPort inPort)
    {
      connections.Add(inPort);
    }

    public bool CanConnect(YPort port2)
    {
      return GetValueType() == port2.GetValueType();
    }

    public void Disconnect(YPort inPort) {
      connections.Remove(inPort);
    }

    public bool HasConnections { get => connections.Count > 0; }

    public bool IsInput { get => direction == PortIO.In; }
    public bool IsOutput { get => direction == PortIO.Out; }
    public bool IsSingle { get => capacity == ConnectionCapacity.Single;  }
    public bool IsMulti { get => capacity == ConnectionCapacity.Multi; }
    public bool IsConnectorVisible { get => isConnectorVisible; }

    public Type GetValueType() {
      return node.GetValueType(portName);
    }

    public object GetValue()
    {
      return node.GetValue(portName);
    }

    public void UpdateConnections(object val) {
      if(IsInput) return;

      foreach (var port in connections)
      {
        port.node.SetValue(port.portName, val);
      }
    }
  }
}
