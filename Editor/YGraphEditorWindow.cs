using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Proteje.Elements;

namespace Proteje.Editor
{
  public class YGraphEditorWindow : EditorWindow
  {
    protected Dictionary<string, Type> menuItems {
      get; set;
    }

    protected YGraph _graph;

    public YGraph baseGraph { get => _graph; set { _graph = value; } }
    private YGraphView _graphView;

    public void Init(YGraph g) {
      _graphView.baseGraph = g;
      _graphView.Load();
    }

    public static void Load(YGraph g)
    {
      var window = GetWindow<YGraphEditorWindow>();
      window.titleContent = new GUIContent("YGraph Editor");
      window.Init(g);
    }

    private void OnEnable() {
      Initialize();
      ConstructGraphView();
      GenerateToolbar();
    }

    private void ConstructGraphView() {

      _graphView = new YGraphView
      {
        name = titleContent.ToString(),
      };

      _graphView.menuItems = menuItems;

      _graphView.StretchToParentSize();
      rootVisualElement.Add(_graphView);
    }

    protected void Initialize() {
      var type = typeof(YNode);
      var types = AppDomain.CurrentDomain.GetAssemblies()
                      .SelectMany(x => x.GetTypes())
                      .Where(x => x.IsClass && type.IsAssignableFrom(x));

      menuItems = new Dictionary<string, Type>();
      foreach (var t in types)
      {
        menuItems.Add("Custom/" + t.Name, t);
      }
    }

    private void GenerateToolbar() {
      var toolbar = new Toolbar();

      toolbar.Add(new Button(() => Execute()) { text = "Execute" });
      toolbar.Add(new Button(() => Save()) { text = "Save" });

      rootVisualElement.Add(toolbar);
    }

    private void Save() {
      EditorUtility.SetDirty(_graphView.baseGraph);
      AssetDatabase.SaveAssets();
    }

    private void Execute() {
      _graphView.Execute();
    }

    private void OnDisabled() {
      rootVisualElement.Remove(_graphView);
    }
  }
}
