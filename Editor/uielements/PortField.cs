using System;
using System.Collections.Generic;
using UnityEngine.UIElements;

namespace Proteje.Editor.UiElements
{
  public class PortField {}
  public class PortField<T> : PortField
  {
    public Action<T> OnValueChanged;
    protected List<VisualElement> elements = new List<VisualElement>();

    protected virtual void Initialize(T initialValue) {}
  }
}
