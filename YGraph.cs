using System.Collections.Generic;
using Proteje.Elements;
using UnityEngine;

namespace Proteje
{
  public class YGraph : ScriptableObject
  {
    public List<YNode> nodes = new List<YNode>();
    public List<YEdge> edges = new List<YEdge>();
    private SceneGraph context;
    public SceneGraph Context { set => context = value;  }

    public void Execute() {
      nodes.FindAll((n) => n.IsStartNode).ForEach(a => a.Exec());
    }

    void Awake() {
      Debug.Log("..." + context);
    }

    void OnEnable()
    {
      Debug.Log("Enable..." + context);
    }

    public void Add(YNode node) {
      node.graph = this;
      nodes.Add(node);
    }

    public void Remove(YNode node) {
      nodes.Remove(node);
    }

    public bool Connect(YNode outNode, string outPortName, YNode inNode, string inPortName) {

      var ok = outNode.ConnectWith(outPortName, inNode, inPortName);
      if(ok) {
        edges.Add(new YEdge
        {
          inNode = inNode,
          inPort = inPortName,
          outNode = outNode,
          outPort = outPortName
        });
      }
      return ok;
    }

    public void Disconnect(YNode outNode, string outPortName, YNode inNode, string inPortName) {
      outNode.Disconnect(outPortName, inNode, inPortName);
      var edge = edges.Find((_edge) => _edge.inNode == inNode && _edge.inPort == inPortName && _edge.outNode == outNode && _edge.outPort == outPortName);
      edges.Remove(edge);
    }
  }
}
