namespace Proteje.Attributes
{

  [System.Serializable]
  public enum PortIO
  {
    In,
    Out,
  }
}
